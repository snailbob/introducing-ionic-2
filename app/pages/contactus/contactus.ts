import {Page, NavController} from 'ionic-framework/ionic';
import {Validators, FormBuilder} from 'angular2/common';

@Page({
  templateUrl: 'build/pages/contactus/contactus.html'
})
export class ContactusPage {
  public contactForm: any;
  constructor(private nav: NavController, fb: FormBuilder) {
    this.contactForm = fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required]
    });
  }

  submitForm() {
    debugger;
  }
}
